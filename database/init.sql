-- Table: public.t_client_portfolio

-- DROP TABLE public.t_client_portfolio;

CREATE TABLE public.t_user
(
  user_iident bigint NOT NULL,
  user_name character varying(255),
  user_email character varying(255),
  user_birthdate timestamp without time zone,
  user_cstatu character(1),
  audit_dfecope timestamp without time zone,
  audit_dfecre timestamp without time zone,
  audit_vobs character varying(255),
  audit_vuser character varying(255),
  CONSTRAINT pk_user_id PRIMARY KEY (user_iident)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.t_user
  OWNER TO postgres;
COMMENT ON COLUMN public.t_user.user_iident IS 'Id de tabla';
COMMENT ON COLUMN public.t_user.user_name IS 'Id de la persona ejecutivo';
COMMENT ON COLUMN public.t_user.user_email IS 'Id de la persona contratante';
COMMENT ON COLUMN public.t_user.user_birthdate IS 'Estado del registro';
COMMENT ON COLUMN public.t_user.user_cstatu IS 'Estado del registro';

COMMENT ON COLUMN public.t_user.audit_dfecope IS 'Campo de auditoria - fecha de actualizacion del registro';
COMMENT ON COLUMN public.t_user.audit_dfecre IS 'Campo de auditoria - fecha de creacion del registro';
COMMENT ON COLUMN public.t_user.audit_vobs IS 'Campo de auditoria - observacion ';
COMMENT ON COLUMN public.t_user.audit_vuser IS 'Campo de auditoria - usuario';

CREATE SEQUENCE public.sq_users
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
  ALTER TABLE public.t_user
  OWNER TO postgres;

-- tabla address

  CREATE TABLE public.t_address
(
  addr_iident bigint NOT NULL,
  addr_street character varying(255),
  addr_state character varying(255),
  addr_city character varying(255),
  addr_country character varying(255),
  addr_cstatu character(1),
  addr_zip character varying(255),
  user_iident bigint NOT NULL,
  audit_dfecope timestamp without time zone,
  audit_dfecre timestamp without time zone,
  audit_vobs character varying(255),
  audit_vuser character varying(255),
  CONSTRAINT pf_address_id PRIMARY KEY (addr_iident),
  CONSTRAINT fk_user_address FOREIGN KEY (user_iident)
      REFERENCES public.t_user (user_iident) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
COMMENT ON COLUMN public.t_address.addr_iident IS 'Id de tabla';
COMMENT ON COLUMN public.t_address.addr_street IS 'Campo de Calle';
COMMENT ON COLUMN public.t_address.addr_state IS 'Campo de Estado';
COMMENT ON COLUMN public.t_address.addr_city IS 'Campo de ciudad';
COMMENT ON COLUMN public.t_address.addr_country IS 'Campo de Pais';
COMMENT ON COLUMN public.t_address.addr_zip IS 'Campo Codigo ZIP';
COMMENT ON COLUMN public.t_address.addr_cstatu IS 'Estado del registro';
COMMENT ON COLUMN public.t_address.user_iident IS 'Id de tabla';


COMMENT ON COLUMN public.t_address.audit_dfecope IS 'Campo de auditoria - fecha de actualizacion del registro';
COMMENT ON COLUMN public.t_address.audit_dfecre IS 'Campo de auditoria - fecha de creacion del registro';
COMMENT ON COLUMN public.t_address.audit_vobs IS 'Campo de auditoria - observacion ';
COMMENT ON COLUMN public.t_address.audit_vuser IS 'Campo de auditoria - usuario';

CREATE SEQUENCE public.sq_address
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
  ALTER TABLE public.t_address
  OWNER TO postgres;

select * from t_user 

  

