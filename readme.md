# Project Code Challenge

_Proyecto Backend para el manejo de Usuarios_

_Se encuentra desplegado en AWS en las siguientes rutas:_

```
Swagger del API: http://ec2-54-146-110-129.compute-1.amazonaws.com:8070/swagger-ui.html#/
Portal Zipkin: http://ec2-54-146-110-129.compute-1.amazonaws.com:9411/
```

_Para poder utilizar el API, se adjunta el File exportado de POSTMAN y se encuentra en la siguiente ruta:_

```
code-challenge/code_challenge.postman_collection.json
```


_El portal Zipkin nos permitirá visualizar todas las peticiones enviadas al API_

## Comenzando 🚀

_Para obtener una copia del repositorio solo se necesitaría clonar con el siguiente comando: ._

```
git clone https://azevallosyapias@bitbucket.org/azevallosyapias/code-challenge.git
```

### Pre-requisitos 📋

_Para poder desplegar el proyecto es necesario tener instalado:_

```
- Maven
- Java version 8 o superior
- Docker

```

### Despliegue 🔧

_Para poder iniciar todos los contenedores basta con ejecutar el comando docker-compose_

_Ubicarse en la ruta del file docker-compose.yml (code-challenge/docker-compose.yml) y ejecutar:_

```
docker-compose up -d
```
_Este comando iniciara todos los contenedores necesarios para deployar el API_

_Un ejemplo de salida del comando_

```
Starting api-challenge_api-zipkin_1 ... done
Starting api-challenge_bdpostgres_1 ... done
Starting api-challenge_api-users_1  ... done
```

_Una vez iniciado los contenedores se habilitaran dos rutas_

```
http://localhost:8070/swagger-ui.html#/
http://localhost:9411/zipkin/
```


_Nota: Los contenedores utilizados son los siguientes:_

```
https://hub.docker.com/r/alonsozy/api-user
https://hub.docker.com/r/alonsozy/database-challenge
https://hub.docker.com/r/alonsozy/api-zipkin
```

## Ejecutando las pruebas ⚙️

_Para ejecutar los UnitTest ejecutar el siguiente comando en la ubicacion del pom.xml_

```
mvn test
```

_Para poder ver la cobertura del código utilizando jacoco, ejecutar lo siguiente:_

```
mvn jacoco:prepare-agent test jacoco:report
```

_Este comando analizará y nos dispondrá un html con la cobertura_

_Este file se ubicará en la siguiente ruta:_

```
code-challenge\target\site\jacoco\index.html
```
_Se coberturó un 96% con las pruebas_

## Construido con 🛠️

_Herramientas utilizadas en el desarrollo del proyecto_

```
* Java 8
* Spring Framework
* Spring Boot
* Spring MVC
* Spring JPA
* AOP - para el manejo de Logs
* Zipkin : nos proporciona una web donde se pueden revisar las peticiones enviadas al API
* Swagger: utilizado para la documentacion del contrato del API
* JUnit
* Mockito
* Jacoco: utilizado para la revision y visualizacion de la cobertura del codigo
* JavaDocs: utilizado para documentar el codigo
* BD PostgreSQL 9.5
* Docker
* Docker Compose
* Postman para las pruebas
```


## Autor ✒️

* **Marino Alonso Z.Y. **

_Links:_
* [Linkedin](https://www.linkedin.com/in/marino-alonso-zevallos-yapias-82a200196/)
* Bitbucket : [azevallosyapias](https://bitbucket.org/azevallosyapias/)
* GitHub: [alonsozy](https://github.com/alonsozy)
* DockerHub: [alonsozy](https://hub.docker.com/u/alonsozy)



