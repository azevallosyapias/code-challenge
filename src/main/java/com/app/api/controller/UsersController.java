package com.app.api.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.app.api.dto.TUser;
import com.app.api.entity.User;
import com.app.api.exception.UserNotFoundException;
import com.app.api.service.IUserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("")
@Api(tags = "default")
public class UsersController {

	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	IUserService userService;

	/**
	 * <p> Metodo para retornar la lista de Usuarios de la BD
	 * </p>
	 * @author Alonso
	 * @param void
	 * @return La lista de Usuarios 
	 */
	@ApiOperation(value = "Get all users", notes = "Get all users")
	@ApiResponses({ @ApiResponse(code = HttpServletResponse.SC_OK, message = "OK") })
	@GetMapping(path = "/getusers", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public List<User> listUsers() {
		log.info("GetMapping - value: /getusers");
		return userService.findAllUsers();
	}

	
	/**
	 * <p> Metodo para registrar un Usuario
	 * </p>
	 * @author Alonso
	 * @param Objeto User
	 * @return El usuario registrado
	 */
	@ApiOperation(value = "Create User", notes = "")
	@ApiResponses({
		@ApiResponse(code = HttpServletResponse.SC_CREATED, message ="CREATED"), 
		@ApiResponse(code = HttpServletResponse.SC_METHOD_NOT_ALLOWED, message ="Invalid input") 
	})
	@PostMapping(path = "/createUsers", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public User createUser(@RequestBody User user) {
		log.info("PostMapping - value: /createUsers");
		return userService.createUser(user);
	}

	
	/**
	 * <p> Metodo para obtener un Usuario por Id
	 * </p>
	 * @author Alonso
	 * @param userId : Id del Usuario
	 * @return El usuario registrado con el Id enviado
	 */
	@ApiOperation(value = " ", notes = "Get one User")
	@ApiResponses({ @ApiResponse(code = HttpServletResponse.SC_OK, message = "OK"), 
					@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message ="Invalid user id"), 
					@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message ="User not found") 
	})
	@GetMapping(path = "/getusersById/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public User getUserByID(@PathVariable("userId") Long userId) throws UserNotFoundException{
		log.info("GetMapping - value: /getusersById/"+userId);
		User user=userService.findUser(userId);
		if(null!=user) {
		}else {
			throw new UserNotFoundException("User not found");
		}
		return user;
	}
	
	
	/**
	 * <p> Metodo para actualizar un Usuario, asi como tambien se actualizara los campos de auditoria (Fecha y motivo)
	 * </p>
	 * @author Alonso
	 * @param userId : Id del Usuario, Objeto User
	 * @return El usuario actualizado con el Id enviado
	 */
	@ApiOperation(value = "", notes = "")
	@ApiResponses({ @ApiResponse(code = HttpServletResponse.SC_OK, message = "OK"), 
					@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message ="Invalid user id"), 
					@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message ="User not found") 
	})
	@PutMapping(path = "/updateUsersById/{userId}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public User putUser(@PathVariable("userId") Long userId,@RequestBody User user) throws UserNotFoundException{
		log.info("PutMapping - value: /updateUsersById/"+userId);
		Optional<TUser> bean=userService.findTUser(userId);
		if(bean.isPresent()) {
			user=userService.editUser(userId, user,bean.get());
		}else {
			throw new UserNotFoundException("User not found");
		}
		return user;
	}
	
	
	/**
	 * <p> Metodo para eliminar un Usuario, asi como tambien se actualizara los campos de auditoria (Fecha y motivo)
	 * </p>
	 * @author Alonso
	 * @param userId : Id del Usuario
	 * @return 
	 */
	@ApiOperation(value = "", notes = "")
	@ApiResponses({ @ApiResponse(code = HttpServletResponse.SC_OK, message = "OK"), 
					@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message ="Invalid user id"), 
					@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message ="User not found") 
	})
	@DeleteMapping(path = "/deleteUsersById/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public void deleteUser(@PathVariable("userId") Long userId) throws UserNotFoundException{
		log.info("DeleteMapping - value: /deleteUsersById/"+userId);
		Optional<TUser> bean=userService.findTUser(userId);
		if(bean.isPresent()) {
			userService.deleteUser(bean.get());
		}else {
			throw new UserNotFoundException("User not found");
		}
	}

}
