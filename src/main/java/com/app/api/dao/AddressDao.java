package com.app.api.dao;

import org.springframework.data.repository.CrudRepository;

import com.app.api.dto.TAddress;

public interface AddressDao extends CrudRepository<TAddress, Long> {

}
