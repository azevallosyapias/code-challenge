package com.app.api.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.app.api.dto.TUser;

public interface UserDao extends CrudRepository<TUser, Long> {
	
	List<TUser> findByEstado(String estado);
	
	@Query("SELECT t FROM TUser t where t.user_iident = :id and t.estado= :estado") 
	Optional<TUser> findUserActive(Long id,String estado);
	
}
