package com.app.api.dto;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.app.api.util.Constants.OBSERV;

@Entity
@Table(name = "t_address")
public class TAddress {

	@Id
	@SequenceGenerator(name = "sequence_address", sequenceName = "sq_address", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence_address")
	private Long addr_iident;

	private String addr_street;
	private String addr_state;
	private String addr_city;
	private String addr_country;
	private String addr_cstatu;
	private String addr_zip;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_iident")
	private TUser user;

	private LocalDateTime audit_dfecope;
	private LocalDateTime audit_dfecre;

	@Enumerated(EnumType.STRING)
	private OBSERV audit_vobs;

	private String audit_vuser;

	public TAddress() {
		// TODO Auto-generated constructor stub
	}

	public Long getAddr_iident() {
		return addr_iident;
	}

	public void setAddr_iident(Long addr_iident) {
		this.addr_iident = addr_iident;
	}

	public String getAddr_street() {
		return addr_street;
	}

	public void setAddr_street(String addr_street) {
		this.addr_street = addr_street;
	}

	public String getAddr_state() {
		return addr_state;
	}

	public void setAddr_state(String addr_state) {
		this.addr_state = addr_state;
	}

	public String getAddr_city() {
		return addr_city;
	}

	public void setAddr_city(String addr_city) {
		this.addr_city = addr_city;
	}

	public String getAddr_country() {
		return addr_country;
	}

	public void setAddr_country(String addr_country) {
		this.addr_country = addr_country;
	}

	public String getAddr_cstatu() {
		return addr_cstatu;
	}

	public void setAddr_cstatu(String addr_cstatu) {
		this.addr_cstatu = addr_cstatu;
	}

	public String getAddr_zip() {
		return addr_zip;
	}

	public void setAddr_zip(String addr_zip) {
		this.addr_zip = addr_zip;
	}

	public TUser getUser() {
		return user;
	}

	public void setUser(TUser user) {
		this.user = user;
	}

	public LocalDateTime getAudit_dfecope() {
		return audit_dfecope;
	}

	public void setAudit_dfecope(LocalDateTime audit_dfecope) {
		this.audit_dfecope = audit_dfecope;
	}

	public LocalDateTime getAudit_dfecre() {
		return audit_dfecre;
	}

	public void setAudit_dfecre(LocalDateTime audit_dfecre) {
		this.audit_dfecre = audit_dfecre;
	}

	public OBSERV getAudit_vobs() {
		return audit_vobs;
	}

	public void setAudit_vobs(OBSERV audit_vobs) {
		this.audit_vobs = audit_vobs;
	}

	public String getAudit_vuser() {
		return audit_vuser;
	}

	public void setAudit_vuser(String audit_vuser) {
		this.audit_vuser = audit_vuser;
	}

}
