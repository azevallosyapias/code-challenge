package com.app.api.dto;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.app.api.util.Constants.OBSERV;

@Entity
@Table(name = "t_user")
public class TUser {

	@Id
	@SequenceGenerator(name = "sequence_user", sequenceName = "sq_users", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence_user")
	@Column(name = "user_iident")
	private Long user_iident;

	private String user_name;
	private String user_email;
	private LocalDateTime user_birthdate;
	
	@Column(name = "user_cstatu")
	private String estado;
	private LocalDateTime audit_dfecope;
	private LocalDateTime audit_dfecre;
	
	@Enumerated(EnumType.STRING)
	private OBSERV audit_vobs;
	
	private String audit_vuser;
	
	@OneToOne(mappedBy = "user", cascade = CascadeType.ALL,
    fetch = FetchType.LAZY, optional = false)
	private TAddress address;

	public TUser() {
		// TODO Auto-generated constructor stub
	}

	public Long getUser_iident() {
		return user_iident;
	}

	public void setUser_iident(Long user_iident) {
		this.user_iident = user_iident;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getUser_email() {
		return user_email;
	}

	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}

	public LocalDateTime getUser_birthdate() {
		return user_birthdate;
	}

	public void setUser_birthdate(LocalDateTime user_birthdate) {
		this.user_birthdate = user_birthdate;
	}


	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public LocalDateTime getAudit_dfecope() {
		return audit_dfecope;
	}

	public void setAudit_dfecope(LocalDateTime audit_dfecope) {
		this.audit_dfecope = audit_dfecope;
	}

	public LocalDateTime getAudit_dfecre() {
		return audit_dfecre;
	}

	public void setAudit_dfecre(LocalDateTime audit_dfecre) {
		this.audit_dfecre = audit_dfecre;
	}

	public OBSERV getAudit_vobs() {
		return audit_vobs;
	}

	public void setAudit_vobs(OBSERV audit_vobs) {
		this.audit_vobs = audit_vobs;
	}

	public String getAudit_vuser() {
		return audit_vuser;
	}

	public void setAudit_vuser(String audit_vuser) {
		this.audit_vuser = audit_vuser;
	}

	public TAddress getAddress() {
		return address;
	}

	public void setAddress(TAddress address) {
		this.address = address;
	}

}
