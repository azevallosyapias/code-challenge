package com.app.api.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -39281132442856465L;

	private int id;

	private String name;

	private String email;

	private LocalDateTime birthDate;

	private Address address;

	public User() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDateTime getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDateTime birthDate) {
		this.birthDate = birthDate;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

}
