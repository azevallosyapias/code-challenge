package com.app.api.service;

import java.util.List;
import java.util.Optional;

import com.app.api.dto.TUser;
import com.app.api.entity.User;

public interface IUserService {

	public List<User> findAllUsers();

	public User createUser(User user);

	public User findUser(Long id);

	public User editUser(Long id, User user, TUser bean);

	public Optional<TUser> findTUser(Long id);

	public TUser deleteUser(TUser user);

}
