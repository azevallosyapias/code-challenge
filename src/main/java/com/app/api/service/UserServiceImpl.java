package com.app.api.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.api.dao.AddressDao;
import com.app.api.dao.UserDao;
import com.app.api.dto.TAddress;
import com.app.api.dto.TUser;
import com.app.api.entity.Address;
import com.app.api.entity.User;
import com.app.api.util.Constants;
import com.app.api.util.Constants.OBSERV;

@Service
public class UserServiceImpl implements IUserService {

	@Autowired
	UserDao userDao;
	
	@Autowired
	AddressDao addressDao;

	/**
	 * <p> Metodo para buscar por ID la entidad nativa TUser mapeada con la tabla T_USER
	 * </p>
	 * @author Alonso
	 * @param ID
	 * @return Optional con el objeto TUSER
	 */
	@Override
	public Optional<TUser> findTUser(Long id) {
		return userDao.findUserActive(id,Constants.STATUS.ACTIVO.getCodigo());
	}

	
	/**
	 * <p> Metodo para obtener la lista de Usuarios a retornar en los Endpoints
	 * Se obtendra la entidad de persistencia TUser y se homologara a la entidad de los controllers User
	 * </p>
	 * @author Alonso
	 * @param ID
	 * @return Lista de Usuarios
	 */
	@Override
	public List<User> findAllUsers() {
		List<TUser> list = userDao.findByEstado(Constants.STATUS.ACTIVO.getCodigo());
		List<User> users = new ArrayList<User>();
		list.stream().forEach(x -> users.add(setUser(x)));
		return users;
	}

	/**
	 * <p> Metodo para registrar la entidad User en la tabla T_USER
	 * </p>
	 * @author Alonso
	 * @param Objeto User
	 * @return Entidad User registrada en BD
	 */
	@Override
	public User createUser(User user) {
		TUser bean= new TUser();
		bean.setUser_name(user.getName());
		bean.setUser_email(user.getEmail());
		bean.setUser_birthdate(user.getBirthDate());
		bean.setEstado(Constants.STATUS.ACTIVO.getCodigo());
		bean.setAudit_dfecope(null);
		bean.setAudit_vobs(OBSERV.CREATE);
		bean.setAudit_vuser("ROOT");
		bean.setAudit_dfecre(LocalDateTime.now());

		TAddress ad = new TAddress();
		Address _adAux= user.getAddress();
		ad.setAddr_city(_adAux.getCity());
		ad.setAddr_country(_adAux.getCountry());
		ad.setAddr_cstatu(Constants.STATUS.ACTIVO.getCodigo());
		ad.setAddr_state(_adAux.getState());
		ad.setAddr_street(_adAux.getStreet());
		ad.setAddr_zip(_adAux.getZip());
		ad.setAudit_dfecope(null);
		ad.setAudit_dfecre(LocalDateTime.now());
		ad.setAudit_vobs(OBSERV.CREATE);
		ad.setAudit_vuser("ROOT");
		ad.setUser(bean);
		bean.setAddress(ad);

		bean = userDao.save(bean);
		return setUser(bean);
	}

	/**
	 * <p> Metodo para buscar por id la entidad TUSER con estado Activo (tabla t_user)
	 * </p>
	 * @author Alonso
	 * @param Long ID
	 * @return En caso de encontrar se retorna la entidad User en caso contrario se retorna null
	 */
	@Override
	public User findUser(Long idUser) {
		Optional<TUser> user= userDao.findUserActive(idUser,Constants.STATUS.ACTIVO.getCodigo());
		if(user.isPresent()) {
			return setUser(user.get());
		}else {
			return null;
		}
		
	}
	
	/**
	 * <p> Metodo para editar el registro de la entidad TUSER
	 * </p>
	 * @author Alonso
	 * @param Id, Entidad User, Entidad TUser
	 * @return Se retorna la entidad User actualizada
	 */
	@Override
	public User editUser(Long id, User user , TUser bean) {
		bean.setUser_iident(id);
		bean.setUser_name(user.getName());
		bean.setUser_email(user.getEmail());
		bean.setUser_birthdate(user.getBirthDate());
		//actualizamos la fecha de auditoria de actualizacion
		bean.setAudit_dfecope(LocalDateTime.now());
		bean.setAudit_vobs(OBSERV.UPDATE);
				
		Address ad=user.getAddress();
		if(null!=ad) {
			TAddress address = bean.getAddress();
			address.setAddr_city(ad.getCity());
			address.setAddr_country(ad.getCountry());
			address.setAddr_street(ad.getStreet());
			address.setAddr_state(ad.getState());
			address.setAddr_zip(ad.getZip());
			
			address.setAudit_dfecope(LocalDateTime.now());
			address.setAudit_vobs(OBSERV.UPDATE);
			
			address.setUser(bean);
			bean.setAddress(address);
		}
		
		bean=userDao.save(bean);
		
		return setUser(bean);
	}
	
	/**
	 * <p> Metodo para eliminar(deshabilitar) el registro de la entidad TUSER
	 * </p>
	 * @author Alonso
	 * @param Entidad TUser
	 * @return
	 */
	@Override
	public TUser deleteUser(TUser user) {
		//deshabilitando el estado
		user.setAudit_dfecope(LocalDateTime.now());
		user.setEstado(Constants.STATUS.ELIMINADO.getCodigo());
		user.setAudit_vobs(OBSERV.DELETE);
		user.getAddress().setAudit_dfecope(LocalDateTime.now());
		user.getAddress().setAddr_cstatu(Constants.STATUS.ELIMINADO.getCodigo());
		user.getAddress().setAudit_vobs(OBSERV.DELETE);
		user = userDao.save(user);
		return user;
	}

	/**
	 * <p> Metodo para homologar los valores de la entidad de persistencia(TUser) con la entidad a retornar por los controladores (User)
	 * </p>
	 * @author Alonso
	 * @param Entidad TUser
	 * @return Entidad User
	 */
	public User setUser(TUser bean) {
		User u = new User();
		u.setId(bean.getUser_iident().intValue());
		u.setName(bean.getUser_name());
		u.setEmail(bean.getUser_email());
		u.setBirthDate(bean.getUser_birthdate());

		Optional<com.app.api.dto.TAddress> op = Optional.ofNullable(bean.getAddress());
		if (op.isPresent()) {
			com.app.api.dto.TAddress a = op.get();
			Address address = new Address();
			address.setCity(a.getAddr_city());
			address.setCountry(a.getAddr_country());
			address.setId(a.getAddr_iident().intValue());
			address.setState(a.getAddr_state());
			address.setStreet(a.getAddr_street());
			address.setZip(a.getAddr_zip());
			u.setAddress(address);
		}

		return u;
	}
	
}
