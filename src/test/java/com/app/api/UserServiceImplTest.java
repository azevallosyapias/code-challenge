package com.app.api;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.app.api.dao.UserDao;
import com.app.api.dto.TAddress;
import com.app.api.dto.TUser;
import com.app.api.entity.Address;
import com.app.api.entity.User;
import com.app.api.service.UserServiceImpl;
import com.app.api.util.Constants;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

	@Mock
	UserDao userDaoMock;

	@InjectMocks
	UserServiceImpl userServiceImpl;

	List<TUser> listTUsers;
	@Before
	public void setUp() {
		listTUsers = new ArrayList<TUser>();
		listTUsers.add(getUser());
	}

	@Test
	public void findTUserTest() throws Exception {
		Long id = 1L;
		TUser user = new TUser();
		user.setUser_iident(id);
		Optional<TUser> op = Optional.ofNullable(user);
		when(userDaoMock.findUserActive(id, Constants.STATUS.ACTIVO.getCodigo())).thenReturn(op);

		op = userServiceImpl.findTUser(id);
		assertTrue(op.isPresent());
		assertEquals(op.get().getUser_iident(), 1L);
	}
	
	@Test
	public void findTUserTest_NoData() throws Exception {
		Long id = -1L;
		Optional<TUser> op = Optional.ofNullable(null);
		when(userDaoMock.findUserActive(id, Constants.STATUS.ACTIVO.getCodigo())).thenReturn(op);
		op = userServiceImpl.findTUser(id);
		assertTrue(!op.isPresent());
	}
	
	
	@Test
	public void findAllUsersTest() throws Exception{
		when(userDaoMock.findByEstado(Constants.STATUS.ACTIVO.getCodigo())).thenReturn(listTUsers);
		List<User> lstUser= userServiceImpl.findAllUsers();
		assertTrue(lstUser.size()>0);
		assertEquals(lstUser.get(0).getId(),listTUsers.get(0).getUser_iident().intValue());
		assertEquals(lstUser.get(0).getName(),listTUsers.get(0).getUser_name());
		assertEquals(lstUser.get(0).getEmail(),listTUsers.get(0).getUser_email());
		assertEquals(lstUser.get(0).getBirthDate(),listTUsers.get(0).getUser_birthdate());
		//validando el objeto Address
		assertEquals(lstUser.get(0).getAddress().getId(),listTUsers.get(0).getAddress().getAddr_iident().intValue());
		assertEquals(lstUser.get(0).getAddress().getStreet(),listTUsers.get(0).getAddress().getAddr_street());
		assertEquals(lstUser.get(0).getAddress().getState(),listTUsers.get(0).getAddress().getAddr_state());
		assertEquals(lstUser.get(0).getAddress().getCity(),listTUsers.get(0).getAddress().getAddr_city());
		assertEquals(lstUser.get(0).getAddress().getCountry(),listTUsers.get(0).getAddress().getAddr_country());
		assertEquals(lstUser.get(0).getAddress().getZip(),listTUsers.get(0).getAddress().getAddr_zip());
	}
	
	@Test
	public void findAllUsersTest_NoData() throws Exception{
		when(userDaoMock.findByEstado(Constants.STATUS.ACTIVO.getCodigo())).thenReturn(new ArrayList<>());
		List<User> lstUser= userServiceImpl.findAllUsers();
		assertTrue(lstUser.size()==0);		
	}
	
	@Test
	public void createUserTest() throws Exception{
		TUser tuser = getUser();
		User user = new User();
		user.setName("Marino Alonso");
		user.setEmail("alonsofisi@gmail.com");
		user.setBirthDate(LocalDateTime.of(1991, 11, 6, 12, 35));
		Address ad= new Address();
		ad.setCity("Villa el Salvador");
		ad.setCountry("PERU");
		ad.setState("LIMA");
		ad.setStreet("Calle 12345");
		ad.setZip("LIMA01");
		user.setAddress(ad);
		when(userDaoMock.save(Mockito.any(TUser.class))).thenReturn(tuser);
		User u2 = userServiceImpl.createUser(user);
		assertEquals(u2.getId(), 1L);
		assertEquals(tuser.getAudit_vobs(), Constants.OBSERV.CREATE);
		assertEquals(tuser.getEstado(),Constants.STATUS.ACTIVO.getCodigo());
		assertNull(tuser.getAudit_dfecope());
		assertNull(tuser.getAddress().getAudit_dfecope());
	}
	
	@Test
	public void findUserTest() throws Exception{
		Long id = 1L;
		Optional<TUser> op = Optional.ofNullable(getUser());
		when(userDaoMock.findUserActive(id, Constants.STATUS.ACTIVO.getCodigo())).thenReturn(op);
		User u = userServiceImpl.findUser(id);
		assertEquals(u.getId(), op.get().getUser_iident().intValue());
	}
	
	@Test
	public void findUserTest_NotFound() throws Exception{
		Long id = -1L;
		Optional<TUser> op = Optional.ofNullable(null);
		when(userDaoMock.findUserActive(id, Constants.STATUS.ACTIVO.getCodigo())).thenReturn(op);
		User u = userServiceImpl.findUser(id);
		assertNull(u);
	}
	
	@Test
	public void editUserTest() {
		Long id=1L;
		User u = new User();
		Address _ad= new Address();
		_ad.setCity("Villa el Salvador");
		_ad.setCountry("PERU");
		_ad.setState("LIMA");
		_ad.setStreet("Calle 12345");
		_ad.setZip("LIMA01");
		u.setAddress(_ad);
		TUser user = getUser();
		TAddress ad = new TAddress();
		ad.setAddr_city("Lima");
		ad.setAddr_country("PERU");
		ad.setAddr_iident(1L);
		ad.setAddr_state("LIMA");
		ad.setAddr_street("Calle 12345");
		ad.setAddr_zip("ZIP12345");
		
		user.setAddress(ad);
		when(userDaoMock.save(Mockito.any(TUser.class))).thenReturn(user);
		User u2 = userServiceImpl.editUser(id,u,user);
		assertEquals(u2.getId(), 1L);
		assertEquals(user.getAudit_vobs(), Constants.OBSERV.UPDATE);
		assertEquals(user.getEstado(),Constants.STATUS.ACTIVO.getCodigo());
		assertNotNull(user.getAudit_dfecope());
	}
	
	@Test
	public void deleteUserTest() {
		TUser user = getUser();
		when(userDaoMock.save(Mockito.any(TUser.class))).thenReturn(user);
		user=userServiceImpl.deleteUser(user);
		assertEquals(user.getAudit_vobs(), Constants.OBSERV.DELETE);
		assertEquals(user.getEstado(),Constants.STATUS.ELIMINADO.getCodigo());
		assertNotNull(user.getAudit_dfecope());
	}
	
	@Test
	public void setUserTest() {
		TUser bean = getUser();
		User u= userServiceImpl.setUser(bean);
		
		assertEquals(u.getId(),bean.getUser_iident().intValue());
		assertEquals(u.getName(),bean.getUser_name());
		assertEquals(u.getEmail(),bean.getUser_email());
		assertEquals(u.getBirthDate(),bean.getUser_birthdate());
		
		assertEquals(u.getAddress().getCity(),bean.getAddress().getAddr_city());
		assertEquals(u.getAddress().getCountry(),bean.getAddress().getAddr_country());
		assertEquals(u.getAddress().getId(),bean.getAddress().getAddr_iident().intValue());
		assertEquals(u.getAddress().getState(),bean.getAddress().getAddr_state());
		assertEquals(u.getAddress().getStreet(),bean.getAddress().getAddr_street());
		assertEquals(u.getAddress().getZip(),bean.getAddress().getAddr_zip());
		
	}
	
	
	private TUser getUser(){
		TUser user= new TUser();
		user.setUser_iident(1L);
		user.setUser_name("Marino Alonso");
		user.setUser_email("alonsofisi@gmail.com");
		user.setUser_birthdate(LocalDateTime.of(1991, 11, 6, 12, 35));
		user.setEstado(Constants.STATUS.ACTIVO.getCodigo());
		user.setAudit_vuser("ROOT");
		user.setAudit_vobs(Constants.OBSERV.CREATE);
		user.setAudit_dfecre(LocalDateTime.now());
		user.setAudit_dfecope(null);
		
		TAddress add = new TAddress();
		add.setAddr_city("Villa el Salvador");
		add.setAddr_country("PERU");
		add.setAddr_cstatu(Constants.STATUS.ACTIVO.getCodigo());
		add.setAddr_iident(1L);
		add.setAddr_state("LIMA");
		add.setAddr_street("Calle 12345");
		add.setAddr_zip("LIMA01");
		add.setAudit_vuser("ROOT");
		add.setAudit_vobs(Constants.OBSERV.CREATE);
		add.setAudit_dfecre(LocalDateTime.now());
		add.setAudit_dfecope(null);
		user.setAddress(add);

		return user;
	}

}
