package com.app.api;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.app.api.controller.UsersController;
import com.app.api.dto.TUser;
import com.app.api.entity.Address;
import com.app.api.entity.User;
import com.app.api.service.IUserService;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

@RunWith(SpringRunner.class)
@WebMvcTest(UsersController.class)
public class UsersControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	IUserService userService;

	private String urlListUsers;
	private String urlCreateUser;
	private String urlUserByID;
	private String urlUpdateUser;
	private String urlDeleteUser;
	List<User> lstUsers = new ArrayList<User>();
	

	@Before
	public void setUp() {
		urlListUsers = "/getusers";
		urlCreateUser = "/createUsers";
		urlUserByID = "/getusersById/_id";
		urlUpdateUser = "/updateUsersById/_id";
		urlDeleteUser = "/deleteUsersById/_id";
		llenarList();
	}

	@Test
	public void listUsersTest_Status() throws Exception {

		when(userService.findAllUsers()).thenReturn(lstUsers);
		mvc.perform(get(urlListUsers)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}
	
	@Test
	public void listUsersTest_Data() throws Exception {

		String jsonRpta="[{\"id\":1,\"name\":\"Marino Alonso\",\"email\":\"alonsofisi@gmail.com\",\"birthDate\":\"1991-11-06T12:35:00\",\"address\":{\"id\":1,\"street\":\"Calle 12345\",\"state\":\"Lima\",\"city\":\"Villa el Salvador\",\"country\":\"PERU\",\"zip\":\"LIMA01\"}}]";
		when(userService.findAllUsers()).thenReturn(lstUsers);
		MvcResult result = mvc.perform(get(urlListUsers)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andReturn();
		
		String text=result.getResponse().getContentAsString();
		JsonElement r = JsonParser.parseString(text);
		assertTrue(r.isJsonArray());
		assertEquals(r, JsonParser.parseString(jsonRpta));
	}
	
	@Test
	public void createUserTest_Status() throws Exception{
		String requestJson ="{\"name\":\"Marino Alonso\",\"email\":\"alonsofisi@gmail.com\",\"birthDate\":\"2013-06-11T00:00:00\",\"address\":{\"street\":\"Calle 12345\",\"state\":\"Lima\",\"city\":\"Villa el Salvador\",\"country\":\"PERU\",\"zip\":\"LIMA01\"}}";
		when(userService.createUser(new User())).thenReturn(llenarUsers());
		mvc.perform(post(urlCreateUser)
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON).content(requestJson))
				.andExpect(status().is(HttpStatus.CREATED.value()));
	}
	
	@Test
	//falta
	public void createUserTest_Data() throws Exception{
		System.out.println("**********************");
		String requestJson ="{\"name\":\"Marino Alonso\",\"email\":\"alonsofisi@gmail.com\",\"birthDate\":\"2013-06-11T00:00:00\",\"address\":{\"street\":\"Calle 12345\",\"state\":\"Lima\",\"city\":\"Villa el Salvador\",\"country\":\"PERU\",\"zip\":\"LIMA01\"}}";
		when(userService.createUser(new User())).thenReturn(llenarUsers());
		mvc.perform(post(urlCreateUser)
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON).content(requestJson))
				.andExpect(status().is(HttpStatus.CREATED.value()))
				.andDo(mvcResult -> {
		            String json = mvcResult.getResponse().getContentAsString();
		            System.out.println("qqqqqqqqqqqqq:"+json);
		        });;
	}
	
	
	@Test
	public void getUserByIDTest_Status() throws Exception {

		Long idUser=1L;
		urlUserByID=urlUserByID.replaceAll("_id", idUser.toString());
		when(userService.findUser(1L)).thenReturn(llenarUsers());
		mvc.perform(get(urlUserByID)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andReturn();
	}
	
	@Test
	public void getUserByIDTest_Status400() throws Exception {
	    
		urlUserByID=urlUserByID.replaceAll("_id", "abc");
		mvc.perform(
				get(urlUserByID).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}
	
	@Test
	public void getUserByIDTest_Status404() throws Exception {
		Long idUser=98765L;
		urlUserByID=urlUserByID.replaceAll("_id", idUser.toString());
		when(userService.findUser(idUser)).thenReturn(null);
		mvc.perform(
				get(urlUserByID).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}
	
	@Test
	public void getUserByIDTest_Data() throws Exception {
		String jsonRpta="{\"id\":1,\"name\":\"Marino Alonso\",\"email\":\"alonsofisi@gmail.com\",\"birthDate\":\"1991-11-06T12:35:00\",\"address\":{\"id\":1,\"street\":\"Calle 12345\",\"state\":\"Lima\",\"city\":\"Villa el Salvador\",\"country\":\"PERU\",\"zip\":\"LIMA01\"}}";	
		Long idUser=1L;
		urlUserByID=urlUserByID.replaceAll("_id", idUser.toString());
		when(userService.findUser(idUser)).thenReturn(llenarUsers());
		MvcResult result = mvc.perform(get(urlUserByID)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andReturn();
		
		String text=result.getResponse().getContentAsString();
		JsonElement r = JsonParser.parseString(text);
		assertTrue(r.isJsonObject());
		assertEquals(r, JsonParser.parseString(jsonRpta));
	}

	
	@Test
	public void putUserTest_Status() throws Exception {
		String requestJson ="{\"name\":\"Marino Alonso\",\"email\":\"alonsofisi@gmail.com\",\"birthDate\":\"2013-06-11T00:00:00\",\"address\":{\"street\":\"Calle 12345\",\"state\":\"Lima\",\"city\":\"Villa el Salvador\",\"country\":\"PERU\",\"zip\":\"LIMA01\"}}";
		
		Long idUser=1L;
		urlUpdateUser=urlUpdateUser.replaceAll("_id", idUser.toString());
		when(userService.findTUser(1L)).thenReturn(llenarOptUser());
		mvc.perform(put(urlUpdateUser)
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON).content(requestJson))
				.andExpect(status().isOk())
				.andReturn();
	}
	
	@Test
	public void putUserTest_Status400() throws Exception {
		String requestJson ="{\"name\":\"Marino Alonso\",\"email\":\"alonsofisi@gmail.com\",\"birthDate\":\"2013-06-11T00:00:00\",\"address\":{\"street\":\"Calle 12345\",\"state\":\"Lima\",\"city\":\"Villa el Salvador\",\"country\":\"PERU\",\"zip\":\"LIMA01\"}}";
		
		urlUpdateUser=urlUpdateUser.replaceAll("_id", "abc");
		mvc.perform(put(urlUpdateUser)
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON).content(requestJson))
				.andExpect(status().isBadRequest())
				.andReturn();
	}
	
	@Test
	public void putUserTest_Status404() throws Exception {
		String requestJson ="{\"name\":\"Marino Alonso\",\"email\":\"alonsofisi@gmail.com\",\"birthDate\":\"2013-06-11T00:00:00\",\"address\":{\"street\":\"Calle 12345\",\"state\":\"Lima\",\"city\":\"Villa el Salvador\",\"country\":\"PERU\",\"zip\":\"LIMA01\"}}";
		Long idUser=852369L;
		urlUpdateUser=urlUpdateUser.replaceAll("_id", idUser.toString());
		when(userService.findTUser(idUser)).thenReturn(emptyOptUser());
		mvc.perform(put(urlUpdateUser)
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON).content(requestJson))
				.andExpect(status().isNotFound())
				.andReturn();
	}
	
	@Test
	public void deleteUserTest_Status() throws Exception {
		Long idUser=1L;
		urlDeleteUser=urlDeleteUser.replaceAll("_id", idUser.toString());
		when(userService.findTUser(idUser)).thenReturn(llenarOptUser());
		mvc.perform(delete(urlDeleteUser)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}
	
	@Test
	public void deleteUserTest_Status400() throws Exception {
		urlDeleteUser=urlDeleteUser.replaceAll("_id", "abc");
		when(userService.findTUser(1L)).thenReturn(emptyOptUser());
		mvc.perform(delete(urlDeleteUser)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}
	
	@Test
	public void deleteUserTest_Status404() throws Exception {
		Long idUser=987654L;
		urlDeleteUser=urlDeleteUser.replaceAll("_id", idUser.toString());
		when(userService.findTUser(idUser)).thenReturn(emptyOptUser());
		mvc.perform(delete(urlDeleteUser)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}
	
	
	
	private Optional<TUser> llenarOptUser() {
		TUser user = new TUser();
		
		Optional<TUser> op = Optional.ofNullable(user);
		return op;
	}
	
	private Optional<TUser> emptyOptUser() {
		Optional<TUser> op = Optional.ofNullable(null);
		return op;
	}

	private User llenarUsers() {
		User u1 = new User();
		u1.setBirthDate(LocalDateTime.of(1991, 11, 6, 12, 35));
		u1.setEmail("alonsofisi@gmail.com");
		u1.setId(1);
		u1.setName("Marino Alonso");

		Address address = new Address();
		address.setCity("Villa el Salvador");
		address.setCountry("PERU");
		address.setId(1);
		address.setState("Lima");
		address.setStreet("Calle 12345");
		address.setZip("LIMA01");
		u1.setAddress(address);
		return u1;
		
	}
	public void llenarList() {
		lstUsers.add(llenarUsers());
	}

}
